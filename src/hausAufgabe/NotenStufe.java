package hausAufgabe;

import java.util.Scanner;

public class NotenStufe {

	public static void main(String[] args) {
		
//		In der Java-Klausur k�nnen Sie maximal 120 Punkte erreichen. 
//		Bei 60 Punkten gilt die Klausur als bestanden (4,0). F�r jede weiteren 5 Punkte verbessert sich die Note um eine Notenstufe. 
//		F�r jeweils 5 Punkte weniger verschlechtert sich die Note um eine Notenstufe (bis 5,0). 
//		Aus rechtlichen Gr�nden gibt es keine 4,3 - sondern in diesem Fall auch eine 4,7. Es k�nnen auch halbe Punkte vergeben werden. 
//		 
//		Note	Punkte 
//		5,0	 	0 - 49,5 
//		4,7 	50 - 59,5 
//		4,0		60 - 64,5 
//		3,7 	65 - 69,5 
//		3,3 	70 - 74,5 
//		3,0 	75 - 79,5 
//		2,7		80 - 84,5 
//		2,3 	85 - 89,5
//		2,0 	90 - 94,5 
//		1,7 	95 - 99,5 
//		1,3 	100 - 104,5 
//		1,0 	105 - 120 
//		
//		Implementieren Sie eine Java-Funktion, die f�r eine Punktzahl von 0 bis 120 die entsprechende Note berechnet und zur�ckgibt. 
//		Schreiben Sie eine Java-main-Methode, die f�r alle Punkte von 0 bis 120 in Schritten von 0,5 die zugeh�rige Note 
//		auf dem Bildschirm ausgibt. 
 
		Scanner sc = new Scanner(System.in);
		double punkte = 0;
		double note = 0;
		
		System.out.println("Bitte geben Sie Ihre Punkte ein: ");
		punkte = sc.nextInt();
		
		MyConvert Konvert = new MyConvert();
	    punkte = Konvert.toDouble1_5(punkte);
		
		if (punkte < 0 || punkte > 120) {
			System.out.println("Fehler. Die Punkten m�ssen zvischen 0..120 sein!!!");
		} else {
			if (punkte <= 49.5) {
				System.out.println("Leider haben Sie NICHT bestanden!");
				note = 5.0;
			} else if (punkte <= 59.5) {
				System.out.println("Leider haben Sie NICHT bestanden!");
				note = 4.7;
			} else if (punkte <= 64.5) {
				note = 4.0;
			} else if (punkte <= 69.5) {
				note = 3.7;
			} else if (punkte <= 74.5) {
				note = 3.3;
			} else if (punkte <= 79.5) {
				note = 3.0;
			} else if (punkte <= 84.5) {
				note = 2.7;
			} else if (punkte <= 89.5) {
				note = 2.3;
			} else if (punkte <= 94.5) {
				note = 2.0;
			} else if (punkte <= 99.5) {
				note = 1.7;
			} else if (punkte <= 104.5) {
				note = 1.3;
			} else {
				note = 1.0;
			}
				
			System.out.printf(" %.1f Punkten = Note %.1f",punkte, note );
		}
		sc.close();
	}
	
	

}
