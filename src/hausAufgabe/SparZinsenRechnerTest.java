package hausAufgabe;

import java.util.Scanner;

public class SparZinsenRechnerTest {

	public static void main(String[] args) {
		/**
		 * Programmieren Sie einen Sparzinsrechner, der folgendes leistet: 
		 * der Anwender kann �ber 3 Eingaben das Startkapital, die Laufzeit in Jahren sowie den Zinssatz eingeben. 
		 * Ausgegeben werden soll das Endkapital am Ende der Laufzeit 
		 * 
		 */
		
		Scanner sc = new Scanner(System.in);
		MyConvert Konvert = new MyConvert();
	
		double startKapital;
		int laufzeitzeit;
		double zinsSatz;
		double endKapital;
		
		
		System.out.print("Bitte geben Sie das Startkapital ein (Euro): ");
		startKapital = sc.nextDouble();
	
		System.out.print("Bitte geben Sie die Laufzeit ein (Jahren): ");
		laufzeitzeit = sc.nextByte();
		
		System.out.print("Bitte geben Sie den Zinssatz ein (0.00 %): ");
		zinsSatz = sc.nextDouble();
	
		double neuZinsen;
		endKapital = startKapital;
		for (int i=0; i<laufzeitzeit; i++) {
			neuZinsen = endKapital * zinsSatz / 100;
			endKapital += neuZinsen;
		}
			
		System.out.println();
		System.out.println("Endkapital: " + Konvert.toDouble2(endKapital));

		sc.close();
	}

}
