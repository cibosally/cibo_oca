package hausAufgabe;

import java.util.Scanner;

public class BruttoNettoRechner {

	public static void main(String[] args) {
		/**
		 *  Der Benutzer soll einen Bruttopreis eingeben k�nnen (Kommazahl)
		 *  Ausgegeben wird der Nettopreis und die Mehrwertsteuer
		 * Testdaten: 119 Euro brutto => 100 Euro netto, 19 Euro Mehrwertsteuer
	     */
		
		//Formatierte Ausgabe mit printf
		// %.2f bedeutet, Ausgabe mit 2 Nachkommastellen
		// %n ist ein Zeilenumbruch
		//System.out.println("Bruttopreis: %.2f E%n", brutto);
		 
		MyConvert Konvert = new MyConvert();
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Bitte geben Sie den Bruttopreis ein: ");
			
		double koeficentSteuer = (double)(19/119);
		double steuerZahl;
		double nettoZahl;
		double bruttoZahl;

		bruttoZahl = sc.nextDouble();
	
		steuerZahl = koeficentSteuer * bruttoZahl;
	
		nettoZahl = bruttoZahl - steuerZahl;
		
		System.out.println();
		System.out.println("Steuer: " + Konvert.toDouble2(koeficentSteuer * 100) + " %");
			
		System.out.println("Netto Zahl: " + Konvert.toDouble2(nettoZahl));
		System.out.println("Mehrwertsteuer: " + Konvert.toDouble2(steuerZahl));
		sc.close();
	}

}
