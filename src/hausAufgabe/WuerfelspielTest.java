package hausAufgabe;

public class WuerfelspielTest {

	public static void main(String[] args) {
//		Ein W�rfelspiel  
//		Wir sollen untersuchen, ob wir bei einem Spiel mit zwei W�rfeln gute Chancen auf Gewinn haben. 
//		Gespielt wird mit zwei W�rfeln mit den Augen 1 bis 6, deren Werte addiert werden. 
//		Wir beginnen das Spiel mit 50 Cent. Der Gewinn berechnet sich nach der unten stehenden Auszahlungstabelle.
//		Augensumme Auszahlung Gewinn 
//		12 4facher Einsatz +1,50 Euro 
//		11 3facher Einsatz +1,00 Euro 
//		10 2facher Einsatz +0,50 Euro 
//		7,8,9 Einsatz zur�ck +0,00 Euro 
//		2,3,4,5,6 keine -0,50 Euro 
//		Lohnt es sich, an diesem Spiel teilzunehmen? Simuliere in einer Schleife (etwa mit 1.000 Durchl�ufen), 
//		ob man auf Dauer gewinnt oder eher verliert. Die W�rfelsumme kann man mit Zufallszahlen simulieren. 
//		Gib das erwirtschaftete oder verlorene Geld am Ende aus. 
		
		double geld;
		int zahl, wuerfel1, wuerfel2;

		double geldSumme = 0.5;
		int duerchLaeufen = 1000;

		for (int i = 1; i <= duerchLaeufen; i++) {

			wuerfel1 = (int) (Math.random() * 6) + 1;
			wuerfel2 = (int) (Math.random() * 6) + 1;

			zahl = wuerfel1 + wuerfel2;
			System.out.println(i + ": " + wuerfel1 + " + " + wuerfel2 + " = " + zahl);

			geld = 0;
			if (zahl == 12) {
				geld = 1.5;
			} else if (zahl == 11) {
				geld = 1.0;
			} else if (zahl == 10) {
				geld = 0.5;
			} else if (zahl <= 6) {
				geld = -0.5;
			}
			geldSumme += geld;
		}
		System.out.println("Am Ende haben Sie : " + geldSumme + " Euro");
		
//		boolean b1 = true ^ true;
//		boolean b2 = true ^ false;
//		boolean b3 = false ^ true;
//		boolean b4 = false ^ false;
//
//		System.out.println(b1 + " ; " + b2 + " ; " + b3 + " ; " + b4);
		
	}

}
