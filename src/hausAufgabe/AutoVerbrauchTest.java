package hausAufgabe;

import java.util.Scanner;

public class AutoVerbrauchTest {

	public static void main(String[] args) {
		/**
		 * Programmieren Sie einen Benzinrechner, der folgendes leistet: 
		 * 		der Anwender kann �ber 2 Eingaben die gefahrenen Kilometer und den Verbrauch in Litern angeben. 
		 * 		Ausgegeben werden soll der Verbrauch auf 100 Kilometern.
		 */
		MyConvert Konvert = new MyConvert();
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie die gefahrenen Kilometer ein: ");
		double gefahrenenKm = sc.nextDouble();

		System.out.print("Bitte geben Sie den Verbrauch in Litern ein: ");
		double verbrauchLitern = sc.nextDouble();
		
		double verbrauchKoeficent = verbrauchLitern/gefahrenenKm;
		
		System.out.println("Verbrauch ist  " + Konvert.toDouble2(verbrauchKoeficent * 100) + "  auf 100 Km. " );
		
		sc.close();
	}

}
