package hausAufgabe;

public class MyConvert {
	
	public double toDouble2(double zahl) {
		return (Math.round(zahl*100.)/100.);
	}
	
	public double toDouble1_5(double zahl) {
		double zahl1 = Math.round(zahl);
		if (zahl1 > zahl) {
			zahl1 -= 0.5;
		}
		return (zahl1);
	}
	

}
