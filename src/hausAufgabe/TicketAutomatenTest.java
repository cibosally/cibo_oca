package hausAufgabe;

import java.util.Scanner;
import java.util.Arrays;

public class TicketAutomatenTest {

	public static void main(String[] args) {
//		Schreiben Sie ein Programm, das einen Fahrscheinautomaten des VRR simuliert.  
//		* Zun�chst soll der Benutzer aus einem Men� mit den zur Verf�gung stehenden Tarifzonen (K, A, B, C, D) ausw�hlen k�nnen.  
//		* Anschlie�end kann der Benutzer solange Geld einwerfen, bis der Fahrpreis bezahlt ist. 
//		* Der Automat darf nur g�ltige Scheine (bis 20 �) und M�nzen annehmen.  
//		* Wird zu viel Geld eingeworfen, gibt der Automat Wechselgeld zur�ck.  
//		* Geben Sie auf dem Bildschirm aus, welche M�nzen als Wechselgeld ausgegeben werden.  
//		* Anschlie�end soll der Automat f�r den n�chsten Benutzer wieder einsatzbereit sein. 
//		 
//		Preis�bersicht 
//		 
//		Preisstufe Preis 
//		K 1,40 � 
//		A 2,30 � 
//		B 4,70 � 
//		C 9,60 � 
//		D 11,40 � 
		
		start :
			while (true) {
			Scanner sc = new Scanner(System.in);
			String preisstufe;
			
			System.out.println("Bitte geben Sie die gew�nshte Preisstuffe ein.");
			System.out.println("Tarifzone: K -  1,40 �.");
	        System.out.println("Tarifzone: A -  2,30 �.");
	        System.out.println("Tarifzone: B -  4,70 �.");
	        System.out.println("Tarifzone: C -  9,60 �.");
	        System.out.println("Tarifzone: D - 11,40 �.");  
	        preisstufe = sc.next();
	        int fahrpreis;
	        switch(preisstufe.toUpperCase()) {
	        case "K":
	        	fahrpreis = 140;
	        	break;
	        case "A":
	        	fahrpreis = 230;
	        	break;
	        case "B":
	        	fahrpreis = 470;
	        	break;
	        case "C":
	        	fahrpreis = 960;
	        	break;
	        case "D":
	        	fahrpreis = 1140;
	        	break;
	        default :
	        		fahrpreis = 0;
	        }
	        if (fahrpreis == 0) {
	        	System.out.println("Und�ltige Preisstufe!!!");
	        	System.out.println("");
	        	continue;
	        }
			System.out.printf("Sie haben Preisstufe %s gew�hlt. Bitte zahlen Sie %.2f �.%n", preisstufe, fahrpreis/100.);
			
			int einwurf;
			int restbetrag = fahrpreis;
			int[] gueltigeGeldwerte = {5, 10, 20, 50, 100, 200, 500, 1000, 2000};
			
			
			while (true) {
				do {
					einwurf = sc.nextInt();
				} while (Arrays.binarySearch(gueltigeGeldwerte, einwurf) < 0);
				
				restbetrag -= einwurf;
				if (restbetrag >0) {
					System.out.printf("Sie haben %.2f E gezahlt. Restbetrag: %.2f E %n", einwurf/100., restbetrag/100.);
				} else if (restbetrag < 0) {
					System.out.printf("Sie haben %.2f E gezahlt. Vielen dank. R�ckgeld: %.2f E %n", einwurf/100., -restbetrag/100.);
					break;			
				} else {
					System.out.printf("Sie haben %.2f E gezahlt. Vielen Dank. %n", einwurf/100.);
					continue start;			
				}
				
			} 

			
			int[] muenzen = {200, 100, 50, 10};
			int n;
			restbetrag *= -1;
			for (int muenz : muenzen) { 
				n = (int)(restbetrag/muenz);
				System.out.println( n + " * " + muenz);
				restbetrag -= n*muenz;
			}
			System.out.println("Rest = " + restbetrag);
		
//			int[] muenzen = {200, 100 , 50, 20, 10};
//			restbetrag *= -1;
//			System.out.println("Ausgabe Restgeld:");
////			do {
////				for (int muenze : muenzen) {
////					if (muenze <= restbetrag) {
////						System.out.println(muenze / 100. + " �");
////						restbetrag -= muenze;
////						break;
////					}
////				}
////			} while (restbetrag > 0);
//			
//			do {
//				for (int muenze : muenzen) {
//					if (restbetrag / muenze >= 1) {
//						System.out.println(restbetrag / muenze + " * " + muenze/100. + " �");
//						restbetrag = restbetrag % muenze;
//					}
//				}
//			} while (restbetrag > 0);
			System.out.println("Servus");	
			System.out.println("--------------------");
		}

	}

}