package hausAufgabe;

import java.util.Scanner;

public class ZahlenSortieren {

	public static void main(String[] args) {
		/**
		 * Drei Zahlen sortieren 
		 * Schreiben Sie eine Java-main-Methode, in der die Werte von drei lokalen int-Variablen a, b und c aufsteigend sortiert werden. 
		 * Verwenden Sie als Kontrollstruktur nur die if-Anweisung. Vertauschen Sie die Werte dieser Variablen so, dass zum Schluss a < b < c gilt. 
		 * Pr�fen Sie Ihre Methode mit allen Permutationen der Zahlen 1, 2 und 3 (es gibt insgesamt 6 Kombinationen). 
		 * Nach der Ausf�hrung Ihrer Anweisungen muss in all diesen F�llen also immer a=1, b=2 und b=3 gelten. 
		 * Man kann die Aufgabe mit drei if-Anweisungen (ohne else) l�sen, da im schlimmsten Fall drei Vertauschungen n�tig sind.
		 */
		
		
		Scanner sc = new Scanner(System.in);
		int a, b, c, temp;
		
		System.out.println("Bitte geben Sie 3 Zahlen ein...");
		System.out.print("a = ");
		a = sc.nextInt();
		System.out.print("b = ");
		b = sc.nextInt();
		System.out.print("c = ");
		c = sc.nextInt();

//		int n1 = 0;
//		int n2 = 0;
//		int n3 = 0;
//
//		if (a < b && b < c) {
//			n1 = a;
//			n2 = b;
//			n3 = c;
//		}else 	if (a < c && c < b) {
//			n1 = a;
//			n2 = c;
//			n3 = b;
//		}else	if (b < a && a < c) {
//			n1 = b;
//			n2 = a;
//			n3 = c;
//		}else	if (b < c && c < a) {
//			n1 = b;
//			n2 = c;
//			n3 = a;
//		}else	if (c < a && a < b) {
//			n1 = c;
//			n2 = a;
//			n3 = b;
//		}else	if (c < b && b < a) {
//			n1 = c; 
//			n2 = b;
//			n3 = a;
//		}
//		a = n1;  b = n2;  c = n3;
//
//		a = 3;	b = 2;	c = 1;

		if (b < a) {
			temp = b;	b = a;  a = temp;
		}
		if (c < a) {
			temp = c;	c = a;  a = temp;
		}
		if (c < b) {
			temp = b;	b = c;  c = temp;
		}

		
		System.out.println(" ------------------------- ");
//		System.out.printf("Ergebnisse: " +  a + " < " +  b + " < " + c);
		System.out.printf("Ergebnisse: %d < %d < %d ",a,b,c);
		sc.close();
	}

}
