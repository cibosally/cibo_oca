package hausAufgabe;

import java.util.Scanner;

public class Temperatur {

	public static void main(String[] args) {
/**
 * Umrechnung von Grad Celsius nach Grad Fahrenheit 
 * Erstellen Sie eine Klasse Temperatur mit einer main-Methode, um eine Temperatur gegeben in Grad Celsius in Grad Fahrenheit umzurechnen. 
 * Die Umrechungsformel ist:  		Fahrenheit = 9/5 * Celsius + 32
 * Die beiden Temperaturen sollen jeweils in lokale Variablen celsius und fahrenheit vom Typ double gespeichert werden. 
 * Das Ergebnis der Berechnung  für irgendeinen Temperaturwert celsius  soll in der Form 10.0 Grad Celsius sind 50.0 Grad Fahrenheit. auf dem Bildschirm ausgegeben werden. 
 */
		
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie das Temperatur ein (Grad Velsius): ");
		double tCesius = sc.nextDouble();
		double tFahrenheit = 9/5 * tCesius + 32;
		System.out.printf("%.1f Grad Celsius sind %.1f Grad Fahrenheit.", tCesius,tFahrenheit);
		sc.close();
	}

}
