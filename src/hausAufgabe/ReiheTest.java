package hausAufgabe;

public class ReiheTest {

	public static void main(String[] args) {
//		Grenzwert einer geometrischen Reihe bestimmen 
//		Wir betrachten folgende unendliche Reihe, welche die Summe der Kehrwerte aller 2er-Potenzen berechnet.  
//		 Reihe(1/2�n) = 1/1 + 1/2 + 1/4 + 1/8 + 1/16+
//		Diese Reihe konvergiert gegen einen konstanten Wert. 
//		Implementieren Sie ein Java-Programm, welche diesen Grenzwert bestimmt. 
//		Implementieren Sie dazu eine Schleife, die solange die n�chste 2er-Potenz zum bisherigen Teilergebnis hinzuaddiert, 
//		bis sich das Resultat nicht mehr �ndert. Dies stellt den - vermutlichen - Grenzwert der Reihe dar.
	
		double grenzWertAlt = 0;
		double grenzWertNeu = 0;
		
		// Konstante
		// final double konstWert = 0.00000000001;
		
		double pow2_K = 0;
		int schritte = 0;
		
		while (true) {
			grenzWertAlt = grenzWertNeu;
			pow2_K = Math.pow(2, schritte);
			grenzWertNeu += 1/pow2_K;
			
			schritte++;
			if (grenzWertNeu == grenzWertAlt) {
				break;
			}
		}
			
		System.out.printf("Resultat: %.6f %n", grenzWertNeu);
		System.out.println("Anzahl der Schritte: " + schritte);
	}

}
