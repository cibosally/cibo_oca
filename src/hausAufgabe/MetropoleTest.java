
package hausAufgabe;

import java.util.Scanner;

public class MetropoleTest {

	public static void main(String[] args) {
//				Boolescher Ausdruck zur Definition einer Metropole 
//				Gegeben seien drei Variablen, die Auskunft �ber Eigenschaften einer bestimmten Stadt geben:  
//				boolean istHaupstadt; int anzahlEinwohner; double steuernProEinwohner; 
//				 
//				Dabei gilt folgendes:  
//				* istHauptstadt ist genau dann true, wenn die Stadt eine Hauptstadt ist. 
//				* anzahlEinwohner gibt die Anzahl der Einwohner der Stadt an. 
//				* steuernProEinwohner ist die durchschnittliche monatliche Steuerabgabe pro Einwohner und Monat. 
//				Wir definieren eine Metropole als eine Stadt, die Hauptstadt ist mit mehr als 100 000 Einwohner oder die mehr als 200 000 Einwohner hat 
//				und im Durchschnitt mindestens 720 000 000 Steuereinnahmen hat.  
//				Geben Sie einen Booleschen-Ausdruck mit den drei Variablen in Java an, der genau dann true ist, wenn die Stadt eine Metropole ist.  



		Scanner sc = new Scanner(System.in);
		boolean istHaupstadt = false;
		int anzahlEinwohner = 0;
		double steuernProEinwohner = 0;
		double steuernProEinwohner_Summe = 0;
		double steuernProEinwohner_Durchschnittswert = 0;
		
		int anzahlEinwohnerMin = 200000;
		double steuernProEinwohnerMin = 720000000;
		boolean istMetropole;
		
		System.out.println("Bitte sagen Sie, ob das Stadt ein Haupstadt ist? ");
		istHaupstadt = sc.nextBoolean();
		System.out.println("Bitte geben Sie die Anzahl der Einwohner der Stadt an: ");
		anzahlEinwohner = sc.nextInt();
		System.out.println("Bitte geben Sie die durchschnittliche monatliche Steuerabgabe pro Einwohner: ");
		for (int i = 1; i <=12; i++) {
			System.out.println(i +" Monat: ");
			steuernProEinwohner = sc.nextDouble();
			steuernProEinwohner_Summe += steuernProEinwohner;
		}
		
		steuernProEinwohner_Durchschnittswert = steuernProEinwohner_Summe / 12;
		
//		istHaupstadt = true;
//		anzahlEinwohner = 2;
//		steuernProEinwohner_Durchschnittswert = 73;
		
		istMetropole = istHaupstadt && (anzahlEinwohner >= anzahlEinwohnerMin) && (steuernProEinwohner_Durchschnittswert >= steuernProEinwohnerMin);
		
		System.out.printf("Durchschnittliche: %.2f %n", steuernProEinwohner_Durchschnittswert);
		
		if (istMetropole) {
			System.out.println("Die Stadt ist EINE Hauptstadt!!");
		} else {
			System.out.println("Die Stadt ist **KEINE** Hauptstadt!!");
		}
		sc.close();
	}

}
