package my_PRG.other;

import java.time.DayOfWeek;

public class Test {

	public static void main(String[] args) {

//		= = = = =  _ IF _  = = = = =

		
//	    �bung
//	    Rabattsatz in Abhängigkeit vom Umsatz
//	    Nach folgender Regel
//	    Umsatz (€)     Rabatt (%)
//	    ---------------------
//	    ab 0            0
//	    ab 10.000       3
//	    ab 20.000       5   
//	    ab 30.000       7   
//	    ab 50.000       10
		
		int umsatz = 23_000, rabattSatz = 0;
		
	    if (umsatz < 10_000) { // Bedingung1
	        rabattSatz = 0; // DRY
	    } else if (umsatz < 20_000) { // Bedingung2
	        rabattSatz = 3;
	    } else  if (umsatz < 30_000) { // Bedingung3
	        rabattSatz = 5;
	    } else if (umsatz < 50_000) { // Bedingung4
	        rabattSatz = 7;
	    } else { 					// Bedingung5
		    rabattSatz = 10;
		} 
	    System.out.println("Der Rabattsatz betr�gt: " + rabattSatz);


//		= = = = =  _ Tern�rer Operator: Bedingung ? Dann : Sonst _  = = = = =
	    
	
	    int rabatt = (umsatz < 10_000 ? 0 : (umsatz < 20_000 ? 3 : (umsatz < 30_000 ? 5 : (umsatz < 50_000 ? 7 : 10))));  
	    String st= umsatz < 10000 ? "0%" : (umsatz < 20000) ? "3%" : (umsatz < 30000) ?  "5%" : "7%"; 
	    System.out.println(rabatt + " , " + st);
	    
	    umsatz = 50;
	    int r = umsatz < 10 ? 0 : umsatz <20 ? 3 : umsatz <30 ? 5 : umsatz <50 ? 7 : 10 ;
	    System.out.println(r);
	    
	    String st1 = "1";
	    String st2 = "4";
	    char c = '7';
	    System.out.println(st1+st2);
	    System.out.println(1+c);
	    

		int a1=15, a2=5;
		int k = a1>a2 ? a1 : a2;
		System.out.println(k + " ist gr��e");
		System.out.println(a1>a2 ? a1 : a2);

		int a=6, b=6;
		System.out.println(a < b ? a + " ist kleiner als " + b : a > b ? a + " ist gr��er als " + b : a + " ist gleich " + b);
		System.out.println(a + (a < b ? " ist kleiner als " : a > b ? " ist gr��er als " : " ist gleich ") + b);

		
//		* * * * *  _ SWITCH/CASE _  * * * * *

		switch (3) {
		case 1: 
			System.out.print(1);
			break;
		case 2: 
			System.out.print(2);
			break;
		default: 
			System.out.print("def");
			break;
		}
		
		Integer j = 2;
		Short sh = 1;

		switch (j) {
		case 1: 
			System.out.print(1);
			break;
		case 2: 
			System.out.print(2);
			break;
		default: 
			System.out.print("def");
			break;
		}
		
//		int lb = 1;
//		switch (1) {
//			case lb:
//				break;
//
//			default:
//				break;
//		}

		final int LB = 1;
		switch (1) {
			case LB:
				break;

			default:
				break;
		}
		
		
		
//		 int dayOfWeek = 6;
//		    switch (dayOfWeek) {
//		    default:
//		      System.out.println("Wochentag");    
//		      break;
//		    case 0: // für Sonntag
//		      System.out.println("Sonntag");
//		      break;
//		    case 6:
//		      System.out.println("Samstag");
//		    }
		    
//		    �bung: Bitte modifizieren Sie obiges PRG so, dass nur Wochentag oder Wochenende ausgegeben wird.		
		
		System.out.println();
		System.out.println("* * * * * *");

		int dayOfWeek = 0;
		switch (dayOfWeek) {
		    case 0 : 
		    case 6: 
		    	System.out.println("Wochenende");
		    	break;
		    default:
		    	System.out.println("Wochentag");    
			    break;
		}
		
		
			 
	}
}
