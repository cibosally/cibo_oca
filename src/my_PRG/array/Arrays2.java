package my_PRG.array;

import java.util.Scanner;

public class Arrays2 {

	public static void main(String[] args) {

//		String[] intArray = {"3","2","1","0"};
//		System.out.println(intArray);
//
////		int[] intArray1 = new int[] {1,6,3,9,0,5};
////		for (int k : intArray1) {
////			System.out.println(k);
////		}
//		
//		int[] d2Array[] = new int[2][3];
//		d2Array[1][1] = 6;
//
//		for (int i = 0; i < 2; i++) {
//			for (int j = 0; j < 3; j++) {
//				System.out.print(d2Array[i][j] + "  ");
//			}
//			System.out.println();
//		}
//		
		
//		= = = = =  _ �bung _  = = = = =
//		Bitte verwenden Sie die obige Schleifen-Schachtelung und fragen Sie die Objecte nach deren L�ngen

		
		
//		Scanner sc = new Scanner(System.in);
//
//		System.out.println("Bitte geben Sie erste Dimension ein.");
//		int dim1 = sc.nextInt();
//		System.out.println("Bitte geben Sie zweite Dimension ein.");
//		int dim2 = sc.nextInt();
//		
//		int[][] dd2Array = new int[dim1][dim2];
//		for (int i = 0; i < dim1; i++) {
//			for (int j = 0; j < dim2; j++) {
//				System.out.print(dd2Array[i][j] + "  ");
//			}
//			System.out.println();
//		}
		
		
		int[][] zweiDArray = new int[2][3];
		zweiDArray[1][2] = 7;
		zweiDArray[0][1] = 3;
		
		int[][] zweiDArraySimetrich = {{2,3,4}, {4,5,2}};
		int[][] zweiDArrayASimetrich = {{9,2,3,4}, {4,5,2}};
		
		for (int i = 0; i < zweiDArray.length; i++) {
			for (int j = 0; j < zweiDArray[i].length; j++) {
				System.out.print(zweiDArray[i][j] + "  ");
			}
			System.out.println();
		}
		
		System.out.println("--------");
	
		for (int i = 0; i < zweiDArrayASimetrich.length; i++) {
			for (int j = 0; j < zweiDArrayASimetrich[i].length; j++) {
				System.out.print(zweiDArrayASimetrich[i][j] + "  ");
			}
			System.out.println();
		}

		System.out.println("---- Christopher Wurm BS----");
		int[][] twoDArray = { { 1, 2, 3, 9 }, { 4, 5, 6 } };
		for (int[] arr : twoDArray) {
			for (int elem : arr) {
				System.out.print(elem + " ");
			}
			System.out.println();
		}

		
		
	}
}
