package my_PRG.array;

import java.util.Random;

public class A5 {

	public static void main(String[] args) {

//		int[][] zweiDArrayASymetrisch = {
//		        {1,2,3,9}, 		// Zeile 0
//		       // {2,2,7,5,9},  // Zeile 1
//		        {},				// Zeile 1
//		        {4,5,6} 		// Zeile 2
//		};
		
		int[][] zweiDArraySymetrisch = {{1,2,3},{4,5,6}};
		for (int i = 0; i < zweiDArraySymetrisch.length; i++) {
			System.out.print(i + ".  ");
		 	for (int j = 0; j < zweiDArraySymetrisch[i].length; j++) {
		 		zweiDArraySymetrisch[i][j] = i*zweiDArraySymetrisch[i].length+j+1;
				System.out.print(zweiDArraySymetrisch[i][j] + "  ");
		   	}			
		   	System.out.println();
		}
		System.out.println();

		//		https://pastebin.com/jmrhSS0U
		
//		double[] db = new double[2];
//		System.out.println("double: " + db[0]);
//		
//		float[] fl = new float[2];
//		System.out.println("float: " + fl[0]);
//		
//		long[] lg = new long[2];
//		System.out.println("long: " + lg[0]);
//		
//		int[] in = new int[2];
//		System.out.println("int: " + in[0]);
//		
//		String[] st = new String[2];
//		System.out.println("String: " + st[0]);
//		
//		System.out.println();
		
		
		
		
//		    ======= �bung: ========
//		    Bitte geben Sie alle Elemente auf dem Bildschirm aus.
		
//		so ist deutlicher
//		https://pastebin.com/ETurZs2F
//		.... als Block  22 - 27
		
//		for (int i = 0; i < zweiDArrayASymetrisch.length; i++) {
//			System.out.print(i + ".  ");
//		 	for (int j = 0; j < zweiDArrayASymetrisch[i].length; j++) {
//				System.out.print(zweiDArrayASymetrisch[i][j] + "  ");
//		   	}			
//		   	System.out.println();
//		}
//		System.out.println();
		
		
		
//		int[][] zweiDArray = {	{1}	};
//		zweiDArray[0][0] = 7;
//		System.out.println(zweiDArray[0][0]);
//		
		
		
//		Übung: Bitte Deklarieren Sie ein 3D-Array, das eine Kantenlänge von 3 hat.
//		Der Name sei DreiDArray
//		int[][][] dreiDArray = new int[3][3][3];
//		dreiDArray[0][0][0] = 4;
//		dreiDArray[0][1][0] = 2;
//		dreiDArray[0][0][1] = 1;
//		dreiDArray[0][0][2] = 4;
//		dreiDArray[1][0][2] = 5;
//		
//		System.out.println("===== FOREACH =====\n");
//		for (int[][] arr_2D : dreyDArray) {
//			for (int[] arr_1D : arr_2D) {
//				for (int elem : arr_1D) {
//					System.out.print(elem + "  ");
//				}
//				System.out.println();
//			}
//			System.out.println();
//		}
//
//		System.out.println("\n===== FOR =====\n");
//		for (int i = 0; i < dreyDArray.length; i++) {
//			for (int j = 0; j < dreyDArray[i].length; j++) {
//				for (int k = 0; k < dreyDArray[i][j].length; k++) {
//					System.out.print(dreyDArray[i][j][k] + "  ");
//				}
//				System.out.println();
//			}
//			System.out.println();
//		}

//		Random rand = new Random();
//		int summe = 0;
//		for (int i = 0; i < dreiDArray.length; i++) {
//			for (int j = 0; j < dreiDArray[i].length; j++) {
//				for (int k = 0; k < dreiDArray[i][j].length; k++) {
//					dreiDArray[i][j][k] = rand.nextInt(4)+1;
//					summe += dreiDArray[i][j][k];
//					System.out.print(dreiDArray[i][j][k] + "  ");
//				}
//				System.out.println();
//			}
//			System.out.println();
//		}
//		
//		System.out.println("Summe: " + summe);

	}
}
