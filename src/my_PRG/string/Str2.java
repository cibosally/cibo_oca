package my_PRG.string;

public class Str2 {

	public static void main(String[] args) {
		System.out.println("Hallo"+1+2);
		System.out.println('a'+'b'+"c"+'a'+'b');
		System.out.println('a'+"c"+('b'));
		
		String alphabet = "";
		for (char zeichen = 'a'; zeichen<='z'; zeichen++) {
			System.out.print(zeichen);
			alphabet += zeichen;
		}
		System.out.println("\n"+alphabet);

//	    Übung:
//	    Bitte erweitern Sie den String alphabet um den Text naja.
//	    Fügen Sie bitte anschließend die Texte vorne und hinten an die entsprechenden Stellen an.
//	    vornenajahinten
//	    dann bitte 5 Leerzeichen hinten und vorne einfügen.
//	    Löschen Sie um Schluß wieder diese Leerzeichen mit der dafür vorgesehenen Methode.
//	    Recherchieren Sie bitte im Internet oder Begleitbuch für Informationen.
		
		String my_text = "";
		char ch = '|';
		System.out.println(ch + my_text + ch);
		my_text += "naja";
		System.out.println(ch + my_text + ch);
		my_text = "vorne" + my_text + "hinten";
		System.out.println(ch + my_text + ch);
		my_text = "     " + my_text + "     ";
		System.out.println(ch + my_text + ch);
		my_text = my_text.trim();
		System.out.println(ch + my_text + ch);
		
		my_text = ".    "+my_text+"    .";
		my_text = my_text.replaceAll(" ", "");
		
		
		System.out.println(ch + my_text + ch);
		System.out.println("\n\t  a b c   ".trim());
		String s = "Hallo";
		System.out.println(s.concat("+!"));
		System.out.println(s);
		s = s.concat("+!");
		System.out.println(s);
		
		
		String str_new = "", str_alt = "animals";
		int lang = str_alt.length();
		for (int i = 0; i <lang; i++) {
			str_new += str_alt.charAt(lang-i-1);
		}
		System.out.println(str_new);
		byte index = 4;
		int index1 = 3;
		long index2 = 2;
		"Hallo".charAt(index);
		"Hallo".charAt(index1);
	//	"Hallo".charAt(index2);
		
		String str3 = "animalsa";
		System.out.println(str3.indexOf('a'));
		System.out.println(str3.indexOf("im"));
		System.out.println(str3.indexOf ('a',2));
		System.out.println(str3.lastIndexOf('a'));
		
		
	
	}
}