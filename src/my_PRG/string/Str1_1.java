package my_PRG.string;

import java.util.Random;

public class Str1_1 {

	public static void main(String[] args) {
			final int KEY_LENGTH = 64;
			String key = "";

			for (int i = 0; i < KEY_LENGTH; i++) {
				int randomInt = getRandomInt(1, 3);

				switch (randomInt) {
				case 1:
					key += getRandomChar(65, 90);
					break;
				case 2:
					key += getRandomChar(97, 122);
					break;
				case 3:
					key += getRandomChar(48, 57);
					break;
				}
			}

			System.out.println(key);
			//System.out.println(key.length());
		}

		private static char getRandomChar(int asciiMinimum, int asciiMaximum) {
			return (char) getRandomInt(asciiMinimum, asciiMaximum);
		}

		private static int getRandomInt(int minimum, int maximum) {
			Random r = new Random();
			return r.nextInt(maximum - minimum + 1) + minimum;
		}
}
