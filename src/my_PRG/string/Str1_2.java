package my_PRG.string;

import java.util.Random;

public class Str1_2 {

	public static void main(String[] args) {
		Random r = new Random();
		char[] key= new char[64];
		
		for (int i = 0; i < key.length; i++) {
			int temp = 0;
			int x = r.nextInt(3);
			
			switch(x) {
			case 0: 
				temp = r.nextInt(26)+97;
				break;
			case 1:
				temp = r.nextInt(26)+65;
				break;
			case 2:
				temp = r.nextInt(10)+48;
				break;
			}
			key[i]=(char)temp;
		}
		System.out.println("Super Secret Password:");
		for (int i = 0; i < key.length; i++) {
			if(i%4==0 & i!=0)System.out.print(" ");
			System.out.print(key[i]);
		}
	}

}
