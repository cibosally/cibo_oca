package my_PRG.string;

public class Str3 {

	public static void main(String[] args) {
		String str = "adswasadwaddfwasafwasasf";
		String string = "animals";
//		System.out.println(string.substring(2, 3));
		if (string.substring(2,2).isEmpty()) System.out.println("ist Empty");
		System.out.println(string.substring(2,2).isEmpty() ? "is Empty" : "is Full");
		System.out.println("Lang: " + (string.substring(2,2)).length());
		
		//System.out.println("!"+string.substring(2,2)+"!");
		
		char ch = 'w';
		int index_new = -1, index_old = -1;
		for (int i = 0; i < str.length(); i++) {
			System.out.print(str.charAt(i) == ch ? i+1 : ",");
		}

		System.out.println();
		while(true) {
			index_new = str.indexOf(ch,index_old);
			if (index_new < 0) break;
			index_old = index_new+1;
			System.out.print(index_old + " ");
		}
		System.out.println("***********");
		
		
		string = new String("animals");
		if (string.charAt(0) == string.toLowerCase().charAt(0)) {
			string = string.toUpperCase().charAt(0) + string.substring(1);
		}
		System.out.println(string);

		
		String my_str = "avatar";
		String my_sub = my_str.substring(0,1);
		if(my_sub!=my_sub.toUpperCase()) {
			my_str = my_sub.toUpperCase()+my_str.substring(1,my_str.length());
		};
		System.out.println(my_str);

		my_str = "avatar";
		my_sub = ""+my_str.charAt(0);

		my_str = my_sub.equals(my_sub.toUpperCase()) ? my_str : my_sub.toUpperCase()+my_str.substring(1);
		System.out.println(my_str);

		
		string = new String("animals");
		if (string.equals(string.toLowerCase().substring(0,1) + string.substring(1)) ) {
				string = string.toUpperCase().substring(0,1) + string.substring(1);
		}
		System.out.println(string);
		
			str= "elise";
			String sub = str.substring(0,1);
			if(sub .contentEquals(sub.toUpperCase())) {
				System.out.println(str);
			}else
				str= sub.toUpperCase().concat(str.substring(1));
				System.out.println(str);
				String s1 = "Hi";
				String s2 = "Hi";
				
		System.out.println("Hi".equals(new String("Hi")));
		System.out.println("Hi" == (new String("Hi")));
		
		System.out.println("********");
		
	}

}
