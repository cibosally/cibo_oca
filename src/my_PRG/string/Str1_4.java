package my_PRG.string;

import java.util.Random;

public class Str1_4 {

	public static void main(String[] args) {
		
		Random r1 = new Random();
		
		int randomWert;
			
		int[] intArray = new int[64];
		char[] charArray = new char[64];
		
		int ctr = 0;
		while (ctr < 64) {
			randomWert = r1.nextInt(130);
			// Mit Hilfe der ASCII-Tabelle auf gültige Werte prüfen
			if (randomWert >= 48 && randomWert <= 57 || (randomWert >= 65 && randomWert <= 90) || (randomWert >= 97 && randomWert <= 122)) {
				intArray[ctr] = randomWert;	// Zuweisung eine gültigen Wertes
				ctr++;
			}
		}
		
		for (int i = 0; i < intArray.length; i++ ) {
			charArray[i] = (char) intArray[i];	// Umwandlung int --> char lt. ASCII-Tabelle
		}
		
		for (int element : intArray) {
			System.out.print(element + " ");
		}
		System.out.println();
		
		for (char element : charArray) {
			System.out.print(element + " ");
		}
	}

}
