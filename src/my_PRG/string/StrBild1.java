package my_PRG.string;

public class StrBild1 {

	public static void main(String[] args) {

//		String str = "Hall";
//		str = str.concat("o");
//		System.out.println(str);
//		StringBuilder sb = new StringBuilder("Hall");
//		sb.append("o");
//		sb = sb.append("o");
//		System.out.println(sb);

		
		String[] komplettName = { 
				"  D   ,   Beispiel,   Ein  ", 
				"M, Müller, Klaus", 
				"W, Meier,   Klara", 
//				"w, Schulze,Rita", 
//				"m, Schmitz, Hubert",
//				"m, Kruse, Sonja", 
//				"m, Schmidt,   Heinrich   " 
				};
		int indexKomma1, indexKomma2;
		String vorName, nachName, sex, hi1, hi2, hi3;

		for (int i = 0; i < komplettName.length; i++) {
			indexKomma1 = komplettName[i].indexOf(",");
			indexKomma2 = komplettName[i].indexOf(",", indexKomma1 + 1);

			sex = komplettName[i].substring(0, indexKomma1).trim().toLowerCase();
			nachName = komplettName[i].substring(indexKomma1 + 1, indexKomma2).trim();
			vorName = komplettName[i].substring(indexKomma2 + 1).trim();

//			~~~~~~~  Verson 1  ~~~~~~~
			hi1 = "Hallo" + (sex.equals("w") ? " Frau " : sex.equals("m") ? " Herr " : " . ");
			System.out.println("1: " + hi1 + vorName + " " + nachName);

//			~~~~~~~  Verson 2  ~~~~~~~
			if (sex.equals("w")) {
				hi2 = " Frau ";
			} else if (sex.equals("m")) {
				hi2 = " Herr ";
			} else {
				hi2 = " .. ";
			}
			hi2 = "Hallo" + hi2;
			System.out.println("2: " + hi2 + vorName + " " + nachName);

//			~~~~~~~  Verson 3  ~~~~~~~
			switch (sex) {
			case "w":
				hi3 = " Frau ";
				break;
			case "m":
				hi3 = " Herr ";
				break;

			default:
				hi3 = " ... ";
				break;
			}
			hi3 = "Hallo" + hi3;
			System.out.println("3: " + hi3 + vorName + " " + nachName + "\n");
		}
//		~~~~~~~  Verson 4  ~~~~~~~

	}

}
