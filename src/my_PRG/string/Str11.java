package my_PRG.string;

public class Str11 {

	public static void main(String[] args) {
		String anredeString[] = new String[3];
		anredeString [0]= "Sehr geehrte Frau";
		anredeString[1] = "Sehr geehrter Herr";
		anredeString[2] = "Hallo";
		
		
		final int arrayRange = 6;
		String[] komplettName = new String[arrayRange];

	      komplettName[0] = "M,Müller, Klaus";
	      komplettName[1] = "W,Meier, Klara";
	      komplettName[2] = "w,Schulze,Rita";
	      komplettName[3] = "m,Schmitz, Hubert";
	      komplettName[4] = "w,Kruse, Sonja";
	      komplettName[5] = "d,Schmidt , Heinrich";
	      
	      for (int i=0;i < komplettName.length; i++) {                                                            
	    	  
	    	  String teiler[] = komplettName[i].split(",");
	    	  String text = "";
	    	  switch (teiler[0].toLowerCase()) {
	    			  case "w":
	    			  case "W":
	    				  text = anredeString[0];
	    				  break;
	    			  case "m":
	    			  case "M":
	    				  text = anredeString[1];
	    				  break;
	    			 default:
	    				  text = anredeString[2];
    					  break;
	    	  }
	    	  System.out.println(text +  " " + teiler[2].trim()+ " " + teiler[1].trim());
  
	      }
	}

}
