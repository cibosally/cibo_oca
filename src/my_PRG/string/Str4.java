package my_PRG.string;

public class Str4 {

	public static void main(String[] args) {

//	    "Müller, Klaus"
//	    "Meier, Klara" 

//	    "Schulze,Rita"  => das fehlende Leerzeichen nach dem Komma ist Absicht.
//	    "Schmitz, Hubert"
//	    "Kruse, Sonja"
		{
			System.out.println("\n\n~~~~~~~~  Salvadors Version 1  ~~~~~~~~~~\n");
			String[][] komplettName = { { "Müller", "Klaus" }, { "Meier", "Klara" }, { "Schulze", "Rita" },
					{ "Schmitz", "Hubert" }, { "Kruse", "Sonja" } };
			for (int i = 0; i < komplettName.length; i++) {
				System.out.println("Der Nachname lautet: " + komplettName[i][0]);
			}

			System.out.println("\n\n~~~~~~~~  Salvadors Version 2  ~~~~~~~~~~\n");
			String[] komplettName1 = { "Ohne Komma", "Müller, Klaus", "Meier, Klara", "Schulze,Rita", "Schmitz, Hubert",
					"Kruse, Sonja" };
			int indexKomma;
			String nachName;
			for (int i = 0; i < komplettName1.length; i++) {
				indexKomma = komplettName1[i].indexOf(",");
				nachName = indexKomma < 0 ? komplettName1[i] : komplettName1[i].substring(0, indexKomma);
				System.out.println("Der Nachname lautet: " + nachName);
			}
		}
		{
			System.out.println("***********");
			String[] namen = { "Müller, Klaus", "Meier, Klara", "Schulze,Rita", "Schmitz, Hubert", "Kruse, Sonja" };
			for (String name : namen) {
				String[] temp = name.split(",");
				temp[0] = temp[0].trim();
				temp[1] = temp[1].trim();
				System.out.println("Der Nachname lautet: " + temp[0]);
			}
			System.out.println("************");

			{
				String[] komplettName = { "Müller, Klaus", "Meier, Klara", "Schulze,Rita", "Schmitz, Hubert",
						"Kruse, Sonja" };

				for (String name : komplettName) {
					String[] splittedName = name.split(",");

					if (splittedName.length == 0) {
						continue;
					}

					System.out.println("Der Nachname lautet: " + splittedName[0].trim());
				}
			}

		}
		{
			System.out.println("~~~~~");
			System.out.println("\n\n~~~~~~~~  Salvadors Version 3  ~~~~~~~~~~\n");
			String[] komplettName1 = { 
					"Müller, Klaus", 
					"Meier, Klara", 
					"Schulze,Rita", 
					"Schmitz, Hubert",
					"Kruse, Sonja", 
					"  Schmidt,   Heinrich   " 
					};
			int indexKomma;
			String vorName;
			for (int i = 0; i < komplettName1.length; i++) {
				indexKomma = komplettName1[i].indexOf(",");
				vorName = komplettName1[i].substring(indexKomma + 1);
				System.out.println("Der Nachname lautet: " + vorName.trim());
			}
		}

		{
			System.out.println("~~~~~~~~~~");
			String[] komplettName1 = { 
					"Müller, Klaus", 
					"Meier,   Klara", 
					"Schulze,Rita", 
					"Schmitz, Hubert",
					"Kruse, Sonja", 
					"  Schmidt,   Heinrich   " 
					};
			int indexKomma;
			String vorName, nachName;
			for (int i = 0; i < komplettName1.length; i++) {
				indexKomma = komplettName1[i].indexOf(",");
				nachName = komplettName1[i].substring(0, indexKomma).trim();
				vorName = komplettName1[i].substring(indexKomma + 1).trim();
				System.out.println("Hallo " + vorName + " " + nachName + "!");
			}
		}
	}

}
