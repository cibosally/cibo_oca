package my_PRG.string;

import java.util.Random;

public class str_BitCoin {

	public static void main(String[] args) {
		{
			// BEGIN Version 1
			{
				System.out.println("~~~~~~ Salvadors Version 1 ~~~~~~~");
				Random random = new Random();
				final int KEY_LANG = 26 + random.nextInt(8);
				int[][] chIndex = { 
						{ 49, 57 },		//1..9
						{ 65, 90 }, 	//A..Z
						{ 97, 122 } };	//a..z

				String my_BitCoin;
				if (random.nextBoolean()) {
					my_BitCoin = "1";
				} else {
					my_BitCoin = "3";
				}
				String my_SysoutBitCoin = my_BitCoin;
				for (int i = 1; i < KEY_LANG; i++) {
					int rd3 = random.nextInt(3);
					int z0 = chIndex[rd3][0];
					int z1 = chIndex[rd3][1];

					char ch = (char) (z0 + random.nextInt(z1 - z0));
					if (ch != 73 && ch != 79 && ch != 108) {
						if (i % 4 == 0) {
							if (i%12 ==0) {
								my_SysoutBitCoin +="\n";
							} else {
								my_SysoutBitCoin += "-";
							}
						}
						my_BitCoin += ch;
						my_SysoutBitCoin += ch;
					} else {
						--i;
					}
				}
				System.out.println("(" + KEY_LANG + ")" + "Bitcoin Key: " + my_BitCoin);
				System.out.println(my_SysoutBitCoin);
			}
			// END Version 1

			// BEGIN Version 2
			System.out.println("~~~~~~ Salvadors Version 2 ~~~~~~~");
			Random random = new Random();
			String strALL_chars = "";
			int[][] chIndex = { 
					{ 49, 57 },		//1..9
					{ 65, 90 }, 	//A..Z
					{ 97, 122 } };	//a..z

			for (int i = 0; i < chIndex.length; i++) {
				String str = "";

				for (int idx = chIndex[i][0]; idx <= chIndex[i][1]; idx++) {
					str += (char) idx;
				}
				strALL_chars += str;
			}
			strALL_chars = strALL_chars.replace("l", "");
			strALL_chars = strALL_chars.replace("I", "");
			strALL_chars = strALL_chars.replace("O", "");

			final int LANG = strALL_chars.length();
			final int KEY_LANG = 26 + random.nextInt(8);
			String my_BitCoin;
			if (random.nextBoolean()) {
				my_BitCoin = "1";
			} else {
				my_BitCoin = "3";
			}
			String my_SysoutBitCoin = my_BitCoin;

			for (int k = 1; k < KEY_LANG; k++) {
				char ch = strALL_chars.charAt(random.nextInt(LANG));
				String str_dot = k != KEY_LANG && k % 4 == 0 ? k % 12 == 0 ? "\n" : "-" : "";
				my_SysoutBitCoin += str_dot + ch;
				my_BitCoin += ch;
			}
			System.out.println("(" + KEY_LANG + ")" + "Bitcoin Key: " + my_BitCoin);
			System.out.println(my_SysoutBitCoin);
		}
		// END Version 2
	}
}
