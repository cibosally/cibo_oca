package my_PRG.string;

import java.util.Random;

public class Str1 {

	public static void main(String[] args) {

//		int i=0;
//		String txt = "Hallo";
//		System.out.println(txt);
//		System.out.println(txt.indexOf('a'));
//		System.out.println("=====");
//		String s1 = new String("Hallo");
//		String s2 = new String("Hallo");
//		if (s1 == s2) System.out.println("ein_Hallo");
//		else System.out.println("KEIN_Hallo");
//		System.out.println();
		
		
//		-------------- 1 Version --------------		
		
		System.out.println("\n========= my Code =============");
		
//		String str_zahlen = "012345789";
//		String str_lower = "abcdefghijklmnopqrstuvwxyz";
//		String str_upper = str_lower.toUpperCase();
//		String str_alle = str_lower + str_zahlen + str_upper;
		
		int[][] chIndex = {{48,57},{65,90},{97,122}};
		String strALL = "";
		for (int i = 0; i < chIndex.length; i++) {
			String str = "";
		
			for (int idx = chIndex[i][0]; idx <= chIndex[i][1]; idx++) {
				str += (char) idx;
			}
			strALL += str;
		}
		String str_alle = strALL;  
		
		
		int lang = str_alle.length();
		String my_schluessel = "";
		
		Random rand = new Random();
		int index;
		char ch;
		String st_dot;
		
		for (int j = 0; j < 64; j++) {
			index = rand.nextInt(lang);
			ch = str_alle.charAt(index);
			st_dot = j!=0 && j!=64 && j%4==0 ? "." : "";
			my_schluessel += st_dot + ch ;
			
		}
		System.out.println("Key: \n"+ my_schluessel);
		
		
//		-------------- 2 Version --------------		
	
	    System.out.println("\n========= short =============");
	    Random r1 = new Random();
	    int randomWert = 0;
	   
	    for (int ctr = 0; ctr < 64;    ) {
	      randomWert = r1.nextInt(122+1);
	      // Mit Hilfe der ASCII-Tabelle auf gültige Werte prüfen
	      if (
	          randomWert >= 48 && randomWert <= 57 || // Ziffern
	          randomWert >= 65 && randomWert <= 90 || // Großbuchstaben
	          randomWert >= 97) {// Kleinbuchstaben
//	        randomWert >= 97 && randomWert <= 122) {// Kleinbuchstaben
	        System.out.print((char) randomWert);
	        ctr++;
	      	String _dot = ctr!=0 && ctr!=64 && ctr%4==0 ? "." : "";
	      	System.out.print(_dot);
	      }
	    }

	}
}
