package mathe;

public class Mathe {
	// Test message
	public static long fibonacci(int zahl) {
		if (zahl <= 2) {
			return zahl -1;
		}
		return fibonacci(zahl -1) + fibonacci(zahl -2);
	}

}
