package basics;

public class TypumwandlungTest {

	public static void main(String[] args) {
		// Typuwanlung = CASTING !!!
		int intVar = 42;  //
		double doubleVar = Math.PI;
//		doubleVar = intVar;	// implizites Casting: der Compiler passt den Datentypen an. Aus int wird double. Stichpunkt: Wiedening (verbreitern)
//		System.out.println(doubleVar);
		
		intVar = (int) doubleVar;  // explizite Casting
		System.out.println(intVar);
		byte b1 = 120;
		byte b2 = 10;
		//byte bb = (byte) (b1 + b2);
		int bb1 = (byte) b1 + b2;
		System.out.println("R1: " + (b1 + b2));
		System.out.println("R2: " + b1 + b2);
		System.out.println(bb1);
		
		byte zahl = 100;
		System.out.println("zahl = "+zahl);
		zahl++; 	//Inkrement-Operator 
					//oder ++zahl;
		zahl += 1;
		zahl = (byte) (zahl +1);
		System.out.println("zahl = "+zahl);
		
		long l = 7678901234L;
		float f = 3.14F;
		//f = l;	//compiliert: implizites casting
		l = (long)f;	//brauchen explizite Casting
		
		System.out.println("l = " + l);
		
		long potenz =  (long)Math.pow(2, 32);	 //casting von dpuble auf int. Achtung: unvorgesehene Ergebnisse m�glich!!!
		System.out.println("potenz = " + potenz);

	}

}
