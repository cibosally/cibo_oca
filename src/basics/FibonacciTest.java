package basics;

public class FibonacciTest {

	public static void main(String[] args) {
		// Ausgabe der ersten 50 Fibonacci-Zahlen
		// Die erste Fibonacci-Zahlen ist 0, die zweite ist 1. Alle weiteren Fibonacci-Zahlen ergeben sich aus der Summe ihrer beiden Vorg�nger.
		

		final int fiboMax = 50;
		long fiboZahl1 = 0;
		long fiboZahl2 = 1;
		long fiboZahl3;
		
		System.out.println("1: 0");
		System.out.println("2: 1");
		
		for (int i = 3; i <= fiboMax; i++) {
			fiboZahl3 = fiboZahl1 + fiboZahl2;
			fiboZahl1 = fiboZahl2;
			fiboZahl2 = fiboZahl3;
			
			System.out.printf("%d: %,d  %n", i, fiboZahl3);
		}
		
		
//		System.out.println("***************************");
//		Fibonacci-Zahlen in einem Array
//		Index in einem Array startet mit 0 !!!
		
		final int ANZAHL = 30;
		System.out.println("1: 0");
		System.out.println("2: 1");
		long[] fiboZahlen = new long[ANZAHL];
		fiboZahlen[0] = 0;
		fiboZahlen[1] = 1;
		
//		Das Feld length gibt beim Array die Anzahl der Elemente zur�ck
		for (int i = 2; i < ANZAHL; i++) {
			fiboZahlen[i] = fiboZahlen[i-1] + fiboZahlen[i-2];
			
			System.out.printf("%d: %,d  %n", i+1, fiboZahlen[i]);
		}
		
//		Ausgabe der Werte des Arrays
//		for (int i = 2; i < fiboZahlen.length; i++) {
//			fiboZahlen[i] = fiboZahlen[i-1] + fiboZahlen[i-2];
//			
//			System.out.printf("%d: %,d  %n", i+1, fiboZahlen[i]);
//		}	
		
//		*************************
//		Vereinfachte Schleife zur Iteration �ber ein Array
//		Erweiterte for-Schleife (for each-Schleife)
//		!!! Nur f�r lesenden Zugriff
		for (long fibo : fiboZahlen) {
			System.out.printf("%,d %n", fibo);
		}
	
	}

}
