package basics;

public class OperatorenTest {

	public static void main(String[] args) {

		// Deklaration von 2 ganzen Zahlen
		int zahl1;
		int zahl2;
		// Zuweisung der Werte
		zahl1 = 42;
		zahl2 = 12;
		// Arithmetische Operationen (rechnen)
		
		int summe = zahl1 + zahl2;
		System.out.println("summe = " + summe);
		int differenz = zahl1 - zahl2;
		System.out.println("differenz = " + differenz);
		int produkt = zahl1 * zahl2;
		System.out.println("produkt = " + produkt);
		int quotient = zahl1 / zahl2;
		System.out.println("quotient = " + quotient);
		
		double dzahl1 = 42;
		double dzahl2 = 12;
		
		
		double quotientDouble = dzahl1 / dzahl2;
		System.out.println("quotientDouble = " + quotientDouble);
		
		// Gr��e: byte < short < int < long < float < double
		
//		boolean b1 = true;
//		byte b2 = 12;
//		int bb = b1+b2;
		
		int modulo = zahl1 % zahl2; 	//ganzzahliger Rest
		System.out.println(zahl1 +  " / " + "" + "modulo(Rest) = " + modulo);
		
		zahl1 = 2;
		zahl2 = 20;
		double potenz = Math.pow(zahl1 , zahl2);
		System.out.println("Potent = " + potenz);
		
	}

}
