package basics;

public class TeilersucheTest {

	public static void main(String[] args) {
		int zahl = 66;

		for (int j = 1; j <= 100; j++) {
			System.out.print(j + ": ");
//			for (int i = 1; i <= j; i++) {
//				if (j % i == 0) {
//					System.out.print(i+ " ");
//				}
//			}
			teilerSuche(j);
			System.out.println();
		}
		
		System.out.println("********************");
		zahl = Integer.MAX_VALUE-1;
		//zahl = 6;
		long start = System.currentTimeMillis();
		teilerSuche(zahl);
		long stop = System.currentTimeMillis();
		System.out.println("");
		System.out.println("Dauert in Sekunden = " + (stop - start) / 1000.0);
	}
	
	public static void teilerSuche (int zahl) {
		System.out.print(zahl + ": ");
		int haelfte = zahl / 2;
		for (int i = 2; i <= haelfte; i++) {
			if (zahl % i == 0) {
				System.out.print(i+ " ");
			}
		}
		
	}
}
