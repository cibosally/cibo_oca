package basics;

import java.util.Scanner;

public class SchaltjahrTest {

	public static void main(String[] args) {
		// Nach Benutzeringabe einer Jahreszahl soll ausgegeben werden, ob das Jahr ein Schaltjahr ist oder nicht
		
		// Julianische Schaltjahrregel: Ein Jahr ist ein Schaltjahr, wenn es durch 4 teilbar ist
		// Beispiel: Eingabe: 2020 => Ausgabe:2020 ist ein Schaltjahr
		// Beispiel: Eingabe: 2021 => Ausgabe:2021 ist kein Schaltjahr
		
//		 Gregorianische Schaltjahrregel:  Ein Jahr ist ein Schaltjahr, wenn es durch 4 teilbar ist. Ausnahme: eine Jahrhundertwende ist 
//		 nur dann ein Schaltjahr, wenn sie durch 400 teilbar ist
//		 Beispiel: Eingabe: 2000 => Ausgabe:2000 ist ein Schaltjahr
//		 Beispiel: Eingabe: 2100 => Ausgabe:2100 ist kein Schaltjahr
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Zahl ein.");
		int zahl = sc.nextInt();

				
		if (zahl % 4 == 0) {
			System.out.println(zahl + " ist EIN Julianische Schaltjahr!");
		} else {
			System.out.println(zahl + " ist KEIN Julianische Schaltjahr!");
		}
		
		System.out.println("--------------------");
		
		
		if (zahl % 4 == 0) {
			if (zahl % 100 == 0 && zahl % 400 != 0) {
				System.out.println(zahl + " ist KEIN Gregorianische Schaltjahr!");
			} else {
				System.out.println(zahl + " ist EIN Gregorianische Schaltjahr!");
			}
		} else {
			System.out.println(zahl + " ist KEIN Gregorianische Schaltjahr!");
		}
		
		sc.close();
	}

}
