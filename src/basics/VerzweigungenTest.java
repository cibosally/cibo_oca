package basics;

public class VerzweigungenTest {

	public static void main(String[] args) {
		int zahl = 0;
		// �berpr�fen, ob zahl gr��er als 0 ist
		// Ohne geschweifte Klammern geh�rt nur die folgende Anweisung zur if-Bedingugn
		// Der Ausdruck in Klammern MUSS ein boolean sein!!!
		
//		if (zahl > 0) {
//			System.out.println("Die Zahl ist gr��er als 0");
//			System.out.println("Die Zahl ist positiv");
//		} else if (zahl < 0) { 
//			System.out.println("Die Zahl ist kleiner als 0");
//			System.out.println("Die Zahl ist negativ");
//		}	else {
//			System.out.println("Die Zahl ist  0");
//		}
		

		zahl = 3;

		//  ---------------------
		//      �berpr�fung, ob eingegebene Zahl positiv und gerade ist
	
//		if ((zahl <= 0)) {
//			System.out.println(zahl + " :ist nicht positiv und gerade.");
//		} else if (zahl % 2 == 0) {
//			System.out.println(zahl + " :ist positiv und gerade.");
//		} else {
//			System.out.println(zahl + " :ist ungerade.");
//		}

/**
 * 		Variante mit Verkn�pfung der beiden Bedingungen mit UND-Operator &
 *		 Short-circuit-Operator && ist performanter: wenn die linke Bedingung false ist, wird die rechte Bedingung nicht mehr ausgewertet
 */		

//		if (zahl > 0 & zahl % 2 == 0) {
//			System.out.println(zahl + " : ist  positiv und gerade.");
//		} else {
//			System.out.println(zahl + " : ist nicht positiv oder ungerade.");
//		}
//		

//		if (zahl > 0) {
//			if (zahl % 2 == 0) {
//				System.out.println(zahl + "  ist  positiv und gerade.");
//			} else {
//				System.out.println(zahl + " ist positiv und ungerade.");
//			}
//		} else {
//			System.out.println(zahl + " ist nicht positiv und gerade.");
//		}

		
		//      ----------------------
		//      �berpr�fung, ob eingegebene Zahl positiv oder gerade ist
		// Vaiante mit dem ODER-Operator | (schort-circuit ||)

//		zahl = 7;
//		if (zahl > 0 | zahl % 2 == 0) {
//			System.out.println(zahl + " : ist  positiv oder gerade.");
//		} else {
//			System.out.println(zahl + " : ist nicht positiv und ungerade.");
//		}
//		
//		/// sing gleische 
//		if (zahl > 0) {
//			System.out.println(zahl + " : ist  positiv oder gerade.");
//		} else {
//			if (zahl % 2 == 0) {
//				System.out.println(zahl + " : ist  positiv oder gerade.");
//			} else
//			System.out.println(zahl + " : ist nicht positiv und gerade.");
//		} 
		
		zahl = -1;
		// -------------------
		// �berpr�fung, ob zahl positiv oder gerade ist (oder neides)

//		if (zahl > 0 && zahl % 2 == 0) {
//			System.out.println(zahl + " ist positiv UND gerade.");
//		} else if (zahl > 0 || zahl % 2 == 0) {		
//			System.out.println(zahl + " ist entweder positiv oder gerade.");
//		} else {
//			System.out.println(zahl + " ist nicht positiv und ungerade.");
//		}
	
		// Variante mit XOR-Operator   ^
		// ... entweder ... oder
		if (zahl > 0 ^ zahl % 2 == 0) {
			System.out.println(zahl + " ist entweder positiv oder gerade.");
		} else if (zahl > 0 && zahl % 2 == 0) {		
			System.out.println(zahl + " ist positiv UND gerade.");
		} else {
			System.out.println(zahl + " ist nicht positiv und ungerade.");
		}
		
	}

}
