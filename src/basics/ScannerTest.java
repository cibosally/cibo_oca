package basics;

import java.util.Scanner;

public class ScannerTest {

	public static void main(String[] args) {
		// Erzeugen eines Scanner-Objekts zur Benutzeingabe
		Scanner sc = new Scanner(System.in);
		// Eingabe durch den Benutzer �ber die Konsole
		
		System.out.println("Bitte geben Sie eine Zahl ein.");
		int zahl = sc.nextInt();
		System.out.println("Zahl: " + zahl);

		System.out.println("Bitte geben Sie eine Zahl ein.");
		double kommaZahl = sc.nextDouble();
		System.out.println("kommaZahl: " + kommaZahl);
		
		System.out.println("Bitte geben Sie eine Zahl ein.");
		String strZahl = sc.next();
		System.out.println("strZahl: " + strZahl);
		sc.close();
		
	}

}
