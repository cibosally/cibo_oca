package basics;

import java.util.Arrays;
import java.util.Random;

public class ArrayTest {

	public static void main(String[] args) {
		
//		int[] lottozahlen = new int[6];
//		Alternative f�r Zufallzehlen zu Math.random();
//		Random r = new Random();
//		for (int i = 0; i < lottozahlen.length; i++) {
//		//	lottozahlen [i] = (int) (Math.random() * 49) + 1;
//			lottozahlen[i] = r.nextInt(9) + 1;
//		}
//		
//		Arrays.sort(lottozahlen);
//		int[] lottozahlen1 = new int[6];
//		int k = 1;
//		lottozahlen1[0] = lottozahlen[0];
//		for (int i = 1; i < lottozahlen.length; i++) {
//			if (lottozahlen[i] != lottozahlen[i-1]) {
//				lottozahlen1[k++] = lottozahlen[i];
//			//	k++;
//			}
//		}		
//		
//		for (int zahl : lottozahlen ) {
//			System.out.println(zahl);
//		}
//		
//		System.out.println("***************");
//		for (int zahl : lottozahlen1 ) {
//			System.out.println(zahl);
//		}

		System.out.println("****************");
		
		int[] lottozahlen = new int[6];
		Random r = new Random();
		boolean doppelt;
		for (int i = 0; i < lottozahlen.length; i++) {
			do {
				lottozahlen[i] = r.nextInt(49) + 1;
				doppelt = false;
				for (int j = 0; j < i; j++) {
					doppelt = (lottozahlen[j] == lottozahlen[i]);
				}
			} while (doppelt);
		}
		Arrays.sort(lottozahlen);
		for (int zahl : lottozahlen ) {
			System.out.println(zahl);
		}
		
//		int[] zahlen = {1, 2, 3, 4, 5, 6};
//		String[] tiere = {"Hund", "Katze", "Maus"};
//		for (String zahl : tiere ) {
//			System.out.println(zahl);
//		}
		
	}
	
}
