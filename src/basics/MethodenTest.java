package basics;

public class MethodenTest {

	public static void main(String[] args) {
		int zahl = 15;
//		long fakultaet = berechneFakultaet(zahl);
//		System.out.println("zahl! = " + fakultaet);
		printFakultaet(zahl);
	}

	public static long berechneFakultaet(int zahl) {
		long fakultaet = 1;
		for (int i = 1; i <= zahl; i++) {
			fakultaet *= i;
		}
		return fakultaet;
	}
	
	public static long berechneFakultaetRekursiv(int zahl) {
//		Abbruchbedingung der Rekursion
		if (zahl == 0) {
			return 1;
		}
		return zahl * berechneFakultaetRekursiv(zahl - 1);
	}
	
	public static void printFakultaet (int zahl) {
		
		System.out.printf("%d! = %,d %n", zahl, berechneFakultaet(zahl));
	}

}
