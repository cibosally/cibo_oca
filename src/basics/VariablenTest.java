package basics;

public class VariablenTest {

	public static void main(String[] args) {
		

		System.out.println("Hallo World!!");
		
		//Declaration von VAriablen (Anmelden beim Betribssystem)
	
		int i = 47058;
		System.out.println("i = " +i); //Ausgabe der Variablen
		
		i = 42; //Erneute Wertzuwisung
		System.out.println("i = " +i); //Ausgabe der Variablen
		
		// Deklaration und Wertzuweisung (Initialisierung) in einer Anweisung
		byte b = 127; 		//(1 Byte) Ganzzahl Wertebereich -128 bis 127
		short s = 12345; 	//(2 Byte) Ganzzahl Wertebereich -32.768 bis 32.767
		int k = 0;			//(4 Byte)
		long l = 3232127; 	//(8 Byte) Ganzzahl Speichergr��e
		// l = 7575757575;  // Compiler-Fehler, da Ganzzahl-Literal gr��er als 4 Byte
		l = 7575757575L;	// !!! L !!!, Compiler wird mitgeteilt, dass Literal als long-Wert gespeichert soll (8 Byte)
							
		float f;	 		//Flie�kommzahl mit einfacher Genauigkeit (7 oder 8 Nachkommastellen) (Speichergr��e 4 Byte)
							//f = 3.14; // Compiler-Fehler, da Flie�kommazahllen-Literale immer 8 Byte gro� sind (double)
		f = 3.14F; 			// Compiler wird mitgeteilt, dass Literal als float-Wert 
		double d = 3.14;	//Flie�kommzahl mit doppelter Genaigkeit (8 Byte)
		boolean istVIP = false;	//TRUE oder FALSE
		System.out.println("istVIP = " + istVIP);
		
		char c; 			// Alphanumerischer Datentyp f�r ein Zeichen (2 Byte - 0 bis 65.535)
		c = '�';
		c = 65; //ESCII Code
		c = 121;
		System.out.println("c = " + c); 
		
		
		// Sonderfall		----------------------------------------
		
		String name; 		// String auch scheinbar primitiv. Der Schein tr�gt: String ist eine Klasse
		name = "Marco";
		
	}

}
