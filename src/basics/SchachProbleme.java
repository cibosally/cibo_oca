package basics;

public class SchachProbleme {

	public static void main(String[] args) {

		double zahl64 = Math.pow(2, 63);
		final double ReiskoTonnen = 0.25/1_000_000;
		double JahrTonnen = 527.4*1_000_000;;
		double TotalReisko = zahl64*ReiskoTonnen;
		int T = (int)(TotalReisko/JahrTonnen);
		
		System.out.printf("2�64 = %.1f", zahl64);
		System.out.println();
		System.out.printf("Total (SCHACH) = %.3f Tonnen ", TotalReisko); 
		System.out.println();
		System.out.printf("Total Jahr = %.3f Tonnen ", JahrTonnen); 
		System.out.println();
		System.out.printf("Jahren = %d ",T);

		System.out.println();
		System.out.println(Math.pow(2, 63));
	}
}
